import fetch from 'node-fetch';
export class messaging {
  private channelAccessToken?: string;
  private channel_id: string;
  private channel_secret: string;
  /**
   * You can get Channel ID and Chnannel Secret here: https://manager.line.biz/account/@{accountName}/setting/messaging-api
   */
  constructor(channel_id: string, channel_secret: string) {
    this.channel_id = channel_id;
    this.channel_secret = channel_secret;
  }
  async init() {
    this.channelAccessToken = await this.getChannelAccessToken(
      this.channel_id,
      this.channel_secret,
    );
    setInterval(async () => {
      this.channelAccessToken = await this.getChannelAccessToken(
        this.channel_id,
        this.channel_secret,
      );
    }, 604800000) //Refreshes channel access token every week.
    ;
  }
  private async getChannelAccessToken(
    client_id: string,
    client_secret: string,
  ): Promise<string> {
    var data = new URLSearchParams();
    data.append("grant_type", "client_credentials");
    data.append("client_id", client_id);
    data.append("client_secret", client_secret);
    var response = await fetch("https://api.line.me/v2/oauth/accessToken", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: data.toString(),
    });
    var res = await response.json();
    if (response.ok == true && "access_token" in res) {
      return res["access_token"];
    } else throw "Coudln't obtain channel access token";
  }
  /** Sends reply using LINE Messaging API
   * ```ts
   * sendReplyAsync(replyToken: "h0ge1oreMipsUmdOlor", messages:[
      {
        "type": "text",
        "text": "Hello world"
      }
    ])
   * ```
  */
  sendReplyAsync(
    replyToken: string,
    messages: Record<string, unknown>[],
  ): Promise<unknown> {
    return new Promise((resolve, reject) => {
      fetch("https://api.line.me/v2/bot/message/reply", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${this.channelAccessToken}`,
        },
        body: JSON.stringify({
          "replyToken": replyToken,
          "messages": messages,
        }),
      }).then((response) => {
        response.ok ? resolve(response) : reject(response);
      });
    });
  }
  /** Sends text reply using LINE Messaging API
   * ```ts
   * sendTextReplyAsync(replyToken: "h0ge1oreMipsUmdOlor", messages:["First message","Second message"])
   * ```
  */
  sendTextReplyAsync(
    replyToken: string,
    messages: string | string[],
  ): Promise<unknown> {
    if (typeof messages == "string") {
      return this.sendReplyAsync(replyToken, [
        {
          "type": "text",
          "text": messages,
        },
      ]);
    } else {
      return this.sendReplyAsync(
        replyToken,
        messages.map((message) => {
          return { "type": "text", "text": message };
        }),
      );
    }
  }
}
