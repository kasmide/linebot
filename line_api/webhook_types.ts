// data is as of 2021/03/18, obtained from https://developers.line.biz/ja/reference/messaging-api/
export type webhookObject = {
  destination: string;
  events: {
    "replyToken"?: string;
    "type": string;
    "mode": string;
    "timestamp": number;
    "source": {
      "type": string;
      "groupID"?: string;
      "roomID"?: string;
      "userID"?: string;
    };
    "message"?: messageObject;
    "unsend"?: {
      "messageId": string;
    };
    "postback"?: {
      "data": string;
      "params"?: {
        "datetime": string;
      };
    };
    "joined"?: {
      "members": {
        "type": "user";
        "userId": string;
      }[];
    };
    "left"?: {
      "members": {
        "type": "user";
        "userId": string;
      }[];
    };
    "videoPlayComplete"?: {
      "trackingId": string;
    };
    "beacon"?: {
      "hwid": string;
      "type": string;
    };
    "link"?: {
      "result": "ok" | "failed";
      "nonce": string;
    };
    "things"?: {
      "deviceId": string;
      "type": "link" | "unlink" | "scenarioResult";
      "result"?: {
        "scenarioId": string;
        "revision": number;
        "startTime": number;
        "endTime": number;
        "resultCode": string;
        "bleNotificationPayload"?: string;
        "errorReason"?: string;
        "actionResults"?: {
          "type": "void" | "binary";
          "data"?: string;
        }[];
      };
    };
  }[];
}

type messageObject = {
  "id": string;
  "type":
    | "text"
    | "image"
    | "video"
    | "audio"
    | "file"
    | "location"
    | "sticker";
  "packageId"?: string; //sticker
  "stickerId"?: string; //sticker
  "stickerResourceType"?: //sticker
    | "STATIC"
    | "ANIMATION"
    | "SOUND"
    | "ANIMATION_SOUND"
    | "POPUP"
    | "POPUP_SOUND"
    | "NAME_TEXT"
    | "PER_STICKER_TEXT";
  "keywords"?: string[]; //sticker
  "fileName"?: string; //file
  "fileSize"?: number; //file
  "duration"?: number; //video, audio
  "title"?: string; //location
  "address"?: string; //location
  "latitude"?: number; //location
  "longtitude"?: number; //location
  "text"?: string; //text
  "emojis"?: { //text
    "index": number;
    "length": number;
    "productID": string;
    "emojiID": string;
  }[];
  "mention"?: { //text
    "mentionees": {
      "index": number;
      "length": number;
      "userId": string;
    }[];
  };
  "contentProvider"?: { //image,video,audio
    "type": "line" | "external";
    "originalContentUrl"?: string;
    "previewImageUrl"?: string;
  };
}