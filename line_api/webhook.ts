import { webhookObject } from "./webhook_types.js";
export class webhook {
  // key is the name of webhook event, array is of the functions which will be called from webhook event.
  private event: { [key: string]: Array<(e: webhookObject) => void> } = {};
  // TODO: Support signature verification
  handle(req, res) {
    const event = this.event;
    const data = [];
    req.on("data", function (chunk) {
      data.push(chunk);
    });
    req.on("end", function () {
      res.setHeader('Content-Type', 'text/plain;charset=UTF-8');
      try {
        const body: webhookObject = JSON.parse(String(Buffer.concat(data)));
        if (
          body["events"] && body["events"][0] &&
          body["events"][0]["type"] in event
        ) {
          event[body["events"][0]["type"]].forEach((callback) => {
            callback(body);
          });
        }
        res.end("りょーかい");
      } catch (e) {
        console.error(e);
        res.end("は？");
      }
    });
  }
  /**
   * eventName: property ["events"][0]["type"] of Webhook event
   */
  addEventListener(eventName: string, callback: (e: webhookObject) => void) {
      if(this.event[eventName] == null) this.event[eventName] = [];
    this.event[eventName].push(callback);
  }
}
