import http from "http";
type routesConfig = {
    routeAddr;
    action: (req: http.IncomingMessage, res: http.ServerResponse) => void;
  }[];
export class requestRouter {
  private routesConfig: routesConfig;
  constructor(routesConfig: routesConfig) {
    this.routesConfig = routesConfig;
  }
  start(port: number) {
    http.createServer((req, res) => {
      if (
        !this.routesConfig.some((element): boolean => {
          if (RegExp(`^${element.routeAddr}$`).test(req.url)) {
            element.action(req, res);
            return true;
          } else return false;
        })
      ) {
        res.statusCode = 404;
        res.setHeader("Content-Type", "text/plain;charset=UTF-8");
        res.end(`No route set for ${decodeURIComponent(req.url)}`);
      }
    }).listen(port);
  }
}
