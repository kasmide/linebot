import fs from "fs";
export class configReader {
  private config: any;
  /**
   * @param elementName: the name of the element to check whether it exists or not.
   * @returns boolean
   */
  exist(elementName: string): boolean {
    if (this.read(elementName) == null) {
      return false;
    }
    return true;
  }
  /**
   * read specified element's value.
   * @param elementName: the path of the element to read value from.
   * path is separeted using ".", like `path.to.the.element`
   * @returns: the value of requested element
   */
  read(elementName: string) {
    if (typeof this.config == "undefined") throw "Configuration not loaded";
    var data = this.config;
    elementName.match(/[^\.]+/g)?.forEach((element) => {
      if (!data[element]) return null;
      data = data[element];
    });
    if (typeof data == "string" && data[0] == "$") { // read env instead if the value is something like $USER
      data = process.env[data.slice(1)];
    }
    return data;
  }
  /**
   * load configurations
   * @param filename: read configurations from specified file.
   */
  load(filename: string) {
    this.config = JSON.parse(fs.readFileSync(filename,"utf-8"));
  }
}