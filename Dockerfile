FROM node:current-alpine
WORKDIR /usr/src/linebot/
COPY . .
RUN npm i
RUN npx tsc
CMD npm start