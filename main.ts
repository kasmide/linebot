#!/bin/env -S deno run --allow-read --allow-net
import { messaging, webhook, webhookObject } from "./line_api/mod.js";
import { configReader } from "./lib/configReader.js";
import pg from "pg";
const { Client } = pg;
import fs from "fs";
import { requestRouter } from "./lib/requestRouter.js";

var config = new configReader();
try {
  config.load("config.json");
} catch {
  throwError("Failed reading/parsing the config file");
}
//config validation
if (
  !config.exist("authorization.channel_id") &&
  !config.exist("authorization.channel_secret")
) {
  throwError("No credentials provided");
}
var database = new Client({
  connectionString: config.read("postgres"),
  ssl: {
    rejectUnauthorized: false,
  },
});
database.connect();

var linebot = new messaging(
  config.read("authorization.channel_id"),
  config.read("authorization.channel_secret"),
);
await linebot.init();
var webhookHandler = new webhook();
webhookHandler.addEventListener("message", messageEvent);
webhookHandler.addEventListener("postback", postbackEvent);
const router = new requestRouter([
  {
    routeAddr: "/webhook",
    action: webhookHandler.handle,
  }
]);
router.start(
  config.exist("webhook.port") ? Number(config.read("webhook.port")) : 2048,
);

function messageEvent(message: webhookObject) {
  if (message["events"][0]["message"] && message["events"][0]["replyToken"]) {
    const replyToken = message["events"][0]["replyToken"];
    if (
      message["events"][0]["message"]["type"] == "text" &&
      message["events"][0]["message"]["text"]
    ) {
      database.query(
        "SELECT outgoingmessage FROM replymessages WHERE $1 ~ incomingmessage AND evaluation = 'regex' OR incomingmessage = $1 AND evaluation = 'exact' ORDER BY priority DESC;",
        [message["events"][0]["message"]["text"]],
      ).then((result) => {
        if (result.rows.length > 0) {
          return database.query(
            "SELECT message FROM messages WHERE id = $1",
            [
              result.rows[0].outgoingmessage[
                Math.floor(Math.random() * result.rows[0].outgoingmessage.length)
              ],
            ],
          );
        } else {
          return Promise.reject(new Error("No matching entry"))
        }
      }).then((result) => {
        return linebot.sendReplyAsync(
          replyToken,
          result.rows[0].message as Record<string, unknown>[],
        );
      }).catch((e) => {
        console.error(e)
        linebot.sendTextReplyAsync(
          replyToken,
          "すみません、応答時にエラーが発生しました。",
        );
      });
    } else {
      linebot.sendTextReplyAsync(
        replyToken,
        "すみません、現時点ではその種類のメッセージには対応していません。",
      );
    }
  }
}

function postbackEvent(message: webhookObject) {
  void (message);
}

function throwError(message: string, exitCode?: number) {
  console.error(`\u001b[31mErr\u001b[0m: ${message}`);
  process.exit(exitCode ? exitCode : 1);
}
